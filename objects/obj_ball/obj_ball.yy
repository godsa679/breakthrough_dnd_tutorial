{
    "id": "b185f5de-8958-4377-822a-7b0d934eed9d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ball",
    "eventList": [
        {
            "id": "563f0853-58ad-4179-b0cc-320577ce5eaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b185f5de-8958-4377-822a-7b0d934eed9d"
        },
        {
            "id": "393c3956-d9c5-47ae-91c2-d5d55b6dced9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "b185f5de-8958-4377-822a-7b0d934eed9d"
        },
        {
            "id": "92bee01b-f399-4b91-961c-6afc1f8729a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b185f5de-8958-4377-822a-7b0d934eed9d"
        },
        {
            "id": "d2548164-4db5-49a0-801f-321f8068f3de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "247a2432-0a0a-49bf-8b78-51b318d3f7d2",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b185f5de-8958-4377-822a-7b0d934eed9d"
        },
        {
            "id": "dc6de55b-9b9f-485d-afce-c79df15c1ea4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "c97a0d89-388d-4cd5-92fc-034189e16b1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b185f5de-8958-4377-822a-7b0d934eed9d"
        },
        {
            "id": "3371ddf5-dd75-421c-8131-c488b7c6fe12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "b185f5de-8958-4377-822a-7b0d934eed9d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2dd9ac97-9d19-4ef9-ba78-8150ad964e62",
    "visible": true
}