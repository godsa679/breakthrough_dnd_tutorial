/// @DnDAction : YoYo Games.Random.Randomize
/// @DnDVersion : 1
/// @DnDHash : 192B96A0
randomize();

/// @DnDAction : YoYo Games.Random.Choose
/// @DnDVersion : 1
/// @DnDHash : 12D8206D
/// @DnDInput : 5
/// @DnDArgument : "var" "colour"
/// @DnDArgument : "option" "c_blue"
/// @DnDArgument : "option_1" "c_aqua"
/// @DnDArgument : "option_2" "c_red"
/// @DnDArgument : "option_3" "c_green"
/// @DnDArgument : "option_4" "c_orange"
colour = choose(c_blue, c_aqua, c_red, c_green, c_orange);

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 1C21B68A
/// @DnDArgument : "colour" "colour"
/// @DnDArgument : "alpha" "false"
image_blend = colour & $ffffff;