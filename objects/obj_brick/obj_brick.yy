{
    "id": "247a2432-0a0a-49bf-8b78-51b318d3f7d2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_brick",
    "eventList": [
        {
            "id": "a5737646-874b-446d-8033-efb6321cda02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "247a2432-0a0a-49bf-8b78-51b318d3f7d2"
        },
        {
            "id": "79949e7f-88de-4181-bf61-67d778ecf233",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "247a2432-0a0a-49bf-8b78-51b318d3f7d2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc97bc12-f073-4c2b-90d8-2cb917357e14",
    "visible": true
}