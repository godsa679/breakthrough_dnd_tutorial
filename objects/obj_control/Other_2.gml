/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 11543B65
/// @DnDArgument : "font" "fnt_game"
/// @DnDSaveInfo : "font" "216d19e1-e9eb-4e3b-8931-f594b3c9e1b0"
draw_set_font(fnt_game);

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 2AA496C3
/// @DnDInput : 3
/// @DnDArgument : "value_2" "3"
/// @DnDArgument : "var" "player_score"
/// @DnDArgument : "var_1" "high_score"
/// @DnDArgument : "var_2" "player_lives"
global.player_score = 0;
global.high_score = 0;
global.player_lives = 3;