{
    "id": "5dd58180-95b4-42ac-adfa-e0eb3509c142",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_control",
    "eventList": [
        {
            "id": "72fe0cb2-bee3-4b62-8983-f548a842454d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "5dd58180-95b4-42ac-adfa-e0eb3509c142"
        },
        {
            "id": "c16162f7-08cc-40d8-9f1c-aed4f5168bd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5dd58180-95b4-42ac-adfa-e0eb3509c142"
        },
        {
            "id": "d32c6856-3b00-480d-b539-ce14690f7bae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5dd58180-95b4-42ac-adfa-e0eb3509c142"
        },
        {
            "id": "0fb087bc-65c2-4797-8e0a-b4158049289c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5dd58180-95b4-42ac-adfa-e0eb3509c142"
        },
        {
            "id": "d2deff84-080f-477a-b510-2ebe30b39b26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "5dd58180-95b4-42ac-adfa-e0eb3509c142"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}