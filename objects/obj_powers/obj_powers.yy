{
    "id": "17ea0562-21fe-4ff2-9844-2311c2dd5241",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_powers",
    "eventList": [
        {
            "id": "bb212f71-a9cf-49d3-9e88-289715d096a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "17ea0562-21fe-4ff2-9844-2311c2dd5241"
        },
        {
            "id": "652d517b-3f8b-4bc0-8211-f7e662222480",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "17ea0562-21fe-4ff2-9844-2311c2dd5241"
        },
        {
            "id": "44fd2e1e-f824-4f36-b46b-67a6690f0d39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "c97a0d89-388d-4cd5-92fc-034189e16b1d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17ea0562-21fe-4ff2-9844-2311c2dd5241"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a35e7b66-311c-44aa-bdfa-42cf5e33f647",
    "visible": true
}