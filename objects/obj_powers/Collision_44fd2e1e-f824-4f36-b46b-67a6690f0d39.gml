/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 4C95A1DA
instance_destroy();

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 311BC40F
/// @DnDArgument : "var" "image_index"
if(image_index == 0)
{
	/// @DnDAction : YoYo Games.Instances.Set_Instance_Var
	/// @DnDVersion : 1
	/// @DnDHash : 488D476D
	/// @DnDApplyTo : other
	/// @DnDParent : 311BC40F
	/// @DnDArgument : "value" "1.5"
	/// @DnDArgument : "instvar" "15"
	with(other) {
	image_xscale = 1.5;
	}

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 606567D6
	/// @DnDApplyTo : other
	/// @DnDParent : 311BC40F
	/// @DnDArgument : "steps" "10*room_speed"
	with(other) {
	alarm_set(0, 10*room_speed);
	
	}
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 56E5AA95
else
{
	/// @DnDAction : YoYo Games.Movement.Set_Speed
	/// @DnDVersion : 1
	/// @DnDHash : 349F4A6F
	/// @DnDApplyTo : b185f5de-8958-4377-822a-7b0d934eed9d
	/// @DnDParent : 56E5AA95
	/// @DnDArgument : "speed" "spd"
	with(obj_ball) speed = spd;
}