{
    "id": "c97a0d89-388d-4cd5-92fc-034189e16b1d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bat",
    "eventList": [
        {
            "id": "b3b07bfe-8337-4306-9e82-5b12d3daffff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c97a0d89-388d-4cd5-92fc-034189e16b1d"
        },
        {
            "id": "3339274a-9403-4ceb-90bf-102c1a7f14be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "c97a0d89-388d-4cd5-92fc-034189e16b1d"
        },
        {
            "id": "39106171-8102-4fa1-b738-de766906e3a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "c97a0d89-388d-4cd5-92fc-034189e16b1d"
        },
        {
            "id": "da8db52d-65a6-424d-972f-1d267118146a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c97a0d89-388d-4cd5-92fc-034189e16b1d"
        },
        {
            "id": "3b9d0eb6-f031-40a6-80b4-003e7c32736f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "c97a0d89-388d-4cd5-92fc-034189e16b1d"
        },
        {
            "id": "4717274a-6ac3-45b5-994e-f9bf6d598bee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "c97a0d89-388d-4cd5-92fc-034189e16b1d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "54291443-7916-4e81-910a-93e0c966068b",
    "visible": true
}