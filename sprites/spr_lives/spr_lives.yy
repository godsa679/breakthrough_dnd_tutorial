{
    "id": "f965200a-5873-409b-87a6-9a3865008aad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 254,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd4d2660-9f78-4862-a1c9-e1893be0c019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f965200a-5873-409b-87a6-9a3865008aad",
            "compositeImage": {
                "id": "8c52e0f1-567c-4094-91de-fb0f22cb5c47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd4d2660-9f78-4862-a1c9-e1893be0c019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9441c018-0427-4803-83a7-4c1069143d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd4d2660-9f78-4862-a1c9-e1893be0c019",
                    "LayerId": "afe1d59e-bef7-451d-9af3-cf412af8543d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "afe1d59e-bef7-451d-9af3-cf412af8543d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f965200a-5873-409b-87a6-9a3865008aad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}