{
    "id": "a35e7b66-311c-44aa-bdfa-42cf5e33f647",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_powers",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d0a1a7f-086b-40b4-a0a5-3a17f171077d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a35e7b66-311c-44aa-bdfa-42cf5e33f647",
            "compositeImage": {
                "id": "d598dd3e-c87c-46c4-922f-5266517ad570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d0a1a7f-086b-40b4-a0a5-3a17f171077d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a738073-9acd-4d33-aea9-09bc8630ff99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d0a1a7f-086b-40b4-a0a5-3a17f171077d",
                    "LayerId": "1cf44a49-69b7-49c9-9b55-7bec524eaba0"
                }
            ]
        },
        {
            "id": "aa0e94e3-8ee4-4eae-ac02-5140ddcec5fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a35e7b66-311c-44aa-bdfa-42cf5e33f647",
            "compositeImage": {
                "id": "1ec47736-69e8-4247-b968-50bd48f01cc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa0e94e3-8ee4-4eae-ac02-5140ddcec5fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bda238dd-9c0a-4ba5-97a1-0d3774b2d201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa0e94e3-8ee4-4eae-ac02-5140ddcec5fe",
                    "LayerId": "1cf44a49-69b7-49c9-9b55-7bec524eaba0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1cf44a49-69b7-49c9-9b55-7bec524eaba0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a35e7b66-311c-44aa-bdfa-42cf5e33f647",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}