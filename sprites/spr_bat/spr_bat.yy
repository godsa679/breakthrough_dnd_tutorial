{
    "id": "54291443-7916-4e81-910a-93e0c966068b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 254,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e71212cd-8e98-409a-b1d7-4f30424458d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54291443-7916-4e81-910a-93e0c966068b",
            "compositeImage": {
                "id": "5a045934-2c3f-4e2b-b295-a66cc1fbc30d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e71212cd-8e98-409a-b1d7-4f30424458d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24ca6d6a-dc50-4ee1-9fbf-1ce235c9b1d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e71212cd-8e98-409a-b1d7-4f30424458d1",
                    "LayerId": "3531c53f-2a9f-4383-9529-a818d0246d45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3531c53f-2a9f-4383-9529-a818d0246d45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54291443-7916-4e81-910a-93e0c966068b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}