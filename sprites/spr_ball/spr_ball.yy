{
    "id": "2dd9ac97-9d19-4ef9-ba78-8150ad964e62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fa4a26f-3cba-4857-baf8-8a6e12e50d26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd9ac97-9d19-4ef9-ba78-8150ad964e62",
            "compositeImage": {
                "id": "3de89ef8-6a4b-4b22-98fd-d4a7795a2d7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa4a26f-3cba-4857-baf8-8a6e12e50d26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebefe634-709b-4325-9f9f-d2b25e7a9e70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa4a26f-3cba-4857-baf8-8a6e12e50d26",
                    "LayerId": "f538ff94-ee4b-4faf-a427-03a728fbd969"
                }
            ]
        },
        {
            "id": "7f7ac647-f341-48ae-8abf-bceee134e02e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd9ac97-9d19-4ef9-ba78-8150ad964e62",
            "compositeImage": {
                "id": "c75170ee-29e8-4488-bb4f-31e6bf40353f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f7ac647-f341-48ae-8abf-bceee134e02e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c47d8d1-6ae3-4a4a-ba6b-0173980cba5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f7ac647-f341-48ae-8abf-bceee134e02e",
                    "LayerId": "f538ff94-ee4b-4faf-a427-03a728fbd969"
                }
            ]
        },
        {
            "id": "9d15c19d-ba6d-49b2-886c-0a1e6f54764f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd9ac97-9d19-4ef9-ba78-8150ad964e62",
            "compositeImage": {
                "id": "43fcbecc-26c7-43ae-b06c-4a9559b02a3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d15c19d-ba6d-49b2-886c-0a1e6f54764f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8488bc44-4da1-42bb-9c42-2f7223d83c32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d15c19d-ba6d-49b2-886c-0a1e6f54764f",
                    "LayerId": "f538ff94-ee4b-4faf-a427-03a728fbd969"
                }
            ]
        },
        {
            "id": "dad66ac4-f833-4133-8830-f55a1982e0c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dd9ac97-9d19-4ef9-ba78-8150ad964e62",
            "compositeImage": {
                "id": "84454fff-7d44-4d48-9170-4c82e13fbc42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dad66ac4-f833-4133-8830-f55a1982e0c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a576cf10-1ffb-4760-b933-e6e379da0ae7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dad66ac4-f833-4133-8830-f55a1982e0c6",
                    "LayerId": "f538ff94-ee4b-4faf-a427-03a728fbd969"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f538ff94-ee4b-4faf-a427-03a728fbd969",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2dd9ac97-9d19-4ef9-ba78-8150ad964e62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}