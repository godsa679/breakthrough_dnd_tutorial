{
    "id": "216d19e1-e9eb-4e3b-8931-f594b3c9e1b0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_game",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "89f8a37d-2f24-4927-92b4-569db8c87dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8bce71d2-a2c9-4beb-bdff-5bc0e71fe45b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 72,
                "y": 134
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "853496ed-824d-4457-8927-43b6a104067d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 31,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 59,
                "y": 134
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "86b948ab-ae00-460e-a971-eb3ca06f26d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 42,
                "y": 134
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "05c2d68c-f085-43f7-a701-6b1177c69e91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 26,
                "y": 134
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "33ae33aa-6d0f-4f79-9e55-03aa70b82b10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 31,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 134
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "6e3ffc83-2c1f-4a00-b0ad-c5a48c4b83b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 227,
                "y": 101
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "999b9527-945b-4305-b41f-f87f632c5d9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 220,
                "y": 101
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b9786df2-ff2a-49fa-bca3-6fe50e1d5348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 210,
                "y": 101
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "473a70c4-5796-4afa-a53b-eff1fd3e7ce0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 200,
                "y": 101
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b1bbbfbc-ec16-4652-9212-172fbd457176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 79,
                "y": 134
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "85b77a02-bc36-4d18-89e5-5037c01a1143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 184,
                "y": 101
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "42e8f734-d8df-4004-ae57-6d2add79ba94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 161,
                "y": 101
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a30cc92c-1cae-42e0-98e1-e96f379f8464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 151,
                "y": 101
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ee9f7178-27d0-4eee-88ec-b64bceece7f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 144,
                "y": 101
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c5871ffd-fd3e-416b-9539-170223e0d4dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 31,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 133,
                "y": 101
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "20021b6e-bcce-4f18-b411-198030dd5a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 118,
                "y": 101
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1826dde6-8f1f-4026-847a-568c2a263d1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 31,
                "offset": 2,
                "shift": 15,
                "w": 9,
                "x": 107,
                "y": 101
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "78fb58b2-2525-43ee-8a74-4a5419f99680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 91,
                "y": 101
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "38078f22-65b2-4231-a655-4c9e387cc8c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 76,
                "y": 101
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "09b55153-d5e1-4ad5-b7fd-af03a5deaa61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 59,
                "y": 101
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "44f43eb8-9d5b-4fd9-8f53-02b064dcc2aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 168,
                "y": 101
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "818ba429-739c-47bc-bb00-88b75d4a5d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 91,
                "y": 134
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f1e1994d-8e95-47de-8b77-44f3de03ddec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 107,
                "y": 134
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "a808577f-6a61-4fea-8ba8-0a6920e2a53f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 122,
                "y": 134
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "808262d8-f40f-4d9d-b6db-4fd360c87407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 200
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d07d3e24-0934-4975-9ebd-eabaaa47943e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 241,
                "y": 167
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "518f9b5d-9c60-4bc7-8735-fdbdc3c0368c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 234,
                "y": 167
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "198d9a04-8659-4818-993b-276c6279dee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 218,
                "y": 167
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "eff2acdf-9a67-498a-acab-27425a0471c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 202,
                "y": 167
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "61630385-38d8-432e-9fc5-003a9eff3b02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 186,
                "y": 167
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4551d07b-3b72-4dac-9771-904486b8b0a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 169,
                "y": 167
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c057209e-1b4d-41cf-b2e7-288821caeee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 31,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 140,
                "y": 167
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7fb457ed-a144-4c94-9a56-fffe93b8b300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 118,
                "y": 167
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "27ec8499-354f-4e5e-aa41-6652041fedf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 98,
                "y": 167
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d71f119f-03e5-494c-9939-da0410c5a98d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 78,
                "y": 167
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4c6f3ef2-574d-4852-ba1c-8585cf0ec941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 58,
                "y": 167
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "57efdbcc-a3a3-470b-b8f0-ecfe429c9e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 40,
                "y": 167
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b93ae4de-3388-4baa-af91-899d60134396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 23,
                "y": 167
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bebced03-d11f-4c56-a232-a7f710677941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 167
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e1c04b8c-e6ba-4f59-ad26-23820ecf0e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 217,
                "y": 134
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c6deff0d-b21b-4781-9343-0ae46378ece7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 210,
                "y": 134
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "8c88adb7-e394-4cbe-8720-382efc00ba0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 195,
                "y": 134
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4341dd9a-bbbb-450d-a105-e4c443624cca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 175,
                "y": 134
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2def050d-1fcd-4288-9bb7-b56d2c671601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 31,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 159,
                "y": 134
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "193ad508-6f6a-4076-8eaf-6aa2d6359872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 31,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 137,
                "y": 134
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "4e59c70d-be40-47f4-8760-7fef5dffe06b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 41,
                "y": 101
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c4728b83-4ee8-4e0e-b52f-812a8df04bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 20,
                "y": 101
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "170ab9a7-eee4-45a5-92b2-e1c4c2ae060c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 101
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d9622b60-eb5a-463b-825a-ebdb219d5566",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 138,
                "y": 35
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "de65ea45-4198-4f3e-8fb0-06a7dac6413f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 107,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2ea6da62-04bb-4961-bdbd-8434a8a39e42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 88,
                "y": 35
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "47df3b5a-5c76-4ad9-a31d-3ca6831f9290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 70,
                "y": 35
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "a58b61ec-fb41-4b38-93f2-9932422f489f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 51,
                "y": 35
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "038fc4a9-6ba5-42ec-acc5-25b4cea2e386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 31,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 30,
                "y": 35
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c886a10a-fe4b-453d-a279-4ec91bf6bc3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 31,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1831727b-4c7c-4193-b8cf-6857d69b0020",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "921cb889-9f1c-4cf8-9863-72905a5fdb85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 31,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "91cfb754-b167-4019-aa8b-5b4bbaf06dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ee6f8f86-e7d0-46dc-b8ae-d351974b9d46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 128,
                "y": 35
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "912ac96a-d5ad-41be-acf5-ce09def95c9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 31,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "08f40ecb-e637-4048-af9e-fcc9c4c27809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6d177bb2-c68d-48cb-845c-154236e4856e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9e5dae9a-dba5-46a9-98e4-e57df599883a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 31,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bdb3e03f-039e-4e81-b685-1c05b9584117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "26df2eb3-e686-4784-88dd-78479cdcf0fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1454d6f9-2866-43e9-a8fa-fd58f99f3b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "59907cda-4094-4754-832d-2dc23cc13547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "451258f5-1bc3-4fbe-bbc7-097c62974f19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "146e1268-3936-46ea-9c6e-c7eeabb51800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d471f5d3-2a39-46f6-97ff-f3569b8cbdc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "99d92e44-66d7-4f0c-9f6f-b7846ba6ef5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 160,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "45cac15d-16b3-4072-ab47-cc9e5359969c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 75,
                "y": 68
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1d0928eb-6ea9-4791-9291-1288f4e3cdf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 176,
                "y": 35
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "06629613-9fc4-447a-a8b4-ee1590f4e55d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 31,
                "offset": -2,
                "shift": 8,
                "w": 8,
                "x": 227,
                "y": 68
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "64820ee2-b830-4b64-8a3a-b3b2e3d966ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 211,
                "y": 68
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7a16e576-ed46-4143-8555-784ab1b3f214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 204,
                "y": 68
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6596153f-4b75-46ce-a580-6fbeec9b5b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 31,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 180,
                "y": 68
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "52388ca2-505b-42c0-88cc-e777c693ef9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 164,
                "y": 68
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b79834b9-77df-45e4-9d62-f5ebd234a314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 147,
                "y": 68
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1f2c2f91-c674-4774-86c3-00978de7d880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 130,
                "y": 68
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2ac6cbe0-cb7e-4196-843d-9ef3c074a986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 114,
                "y": 68
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bc078cd8-c664-411d-9bd1-6988bc7fd422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 102,
                "y": 68
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "aea58274-6bf8-4be2-9e60-cc0ae92124cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 237,
                "y": 68
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2c18e85c-e0cf-4196-8c76-3de0deb7c9d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 91,
                "y": 68
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "4c06ca83-e92c-4a91-a36f-18eea1da2e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 31,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 59,
                "y": 68
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3f631a2d-b503-46aa-8c32-fbf42a097d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 42,
                "y": 68
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ddb91fcf-aaa7-446a-ad54-cb6141646af0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 31,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 19,
                "y": 68
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "110d1dda-16d0-4efc-bb2a-c34bca4ab64e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "51926a60-a3ae-4f1b-98ab-fda7a144b3e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 228,
                "y": 35
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "3ea63b8f-7fa3-4e03-99ea-23a4047f1abb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 213,
                "y": 35
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "50ada703-b748-4771-92aa-e35139198901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 35
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d19fe5a5-3797-4a30-9c4b-5040485292e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 195,
                "y": 35
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0427c988-9c6d-4a0c-88a7-d291a21a03ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 183,
                "y": 35
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1942438f-15e7-42aa-b766-e4fb05f8bcf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 31,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 18,
                "y": 200
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1a16e757-4eae-4082-ab9e-ea9cddc35e35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 31,
                "offset": 5,
                "shift": 26,
                "w": 16,
                "x": 35,
                "y": 200
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "265490f8-b440-419d-8e47-e8d3bce8694a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "620e11cc-b665-465a-b56f-8f2d66d7fd12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "6cd0c610-0f84-4bee-98ec-ee3801b9422d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "213ffc5f-62ab-40db-af8b-884a1c478e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "9742b465-db0b-4bf0-9320-e663921aaa84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "bd697376-8c9c-4536-a823-54dbc219281c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "d15d6fd8-7b75-4ea4-a7da-45983926bba8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "64bf1017-e14c-4c65-b328-b3ae08f28b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "8540b69c-5580-46e8-aa58-8d2e3ba28808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "2dc71a61-ce25-4d28-980c-49a893030fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "ba4d21ec-640a-4c28-a84e-790774fdf517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "70dbb80c-a5c7-4376-88a8-3aa3d0e32ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "c4d5e89d-b1d6-4bfe-8601-eef2873fb2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "675ee266-4374-40e1-b294-93e8daa98a38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "93a35929-0d84-4239-a62f-db01d2782c8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "b3710aac-bd73-4d34-a505-f6eb9b5f72ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "39bfa182-9010-4abd-b9f2-061768aa60dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "aae83bf6-f0b1-4d1c-b77f-fbc2ef3233d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "e2af45c5-0ce9-40f0-84a9-ad30107bbf21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "11ba6d79-13ab-4bb2-9a5c-a218c406bf94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "28a8a1dc-3b23-4e8d-b91d-37be1d28b01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "2bd9a373-f8aa-4f0d-a4a5-417145e02d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "8fe638f6-b138-4c67-878f-bfbb81bee9d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "2840d138-294e-4807-bb5a-aaf810cefd96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "56ccb8f7-cb27-4771-b2df-55f5725e06ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "41f0e08b-6643-4f6d-b3c7-c9c83950739e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "fd38acba-4178-4b44-8206-2ffb3dffa6ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "c5902ef6-40e8-4bb4-9c1d-6b14cc1b9a86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "fad388da-c873-4121-901b-3195336780a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "0de3a287-1253-4ef4-ae29-18fada834104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "722b107b-88d6-435c-9991-1d0338031f0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "3bb191f4-9904-499f-bb3c-3e30d8f9c2f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "4ad74403-8406-4237-b27a-4f1b35e72fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "c0dfde74-0a3f-49ea-a76f-09471ecdf307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "7a31c575-ebfa-4fb8-a998-8764769fe984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "552eca2a-47c6-415e-88ea-2df4cf7a352c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "248e03f4-51ed-4935-85d9-fee36fe20162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "086ed74a-8a2a-41a7-9b31-35a1508f818c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "b3c3bdc3-9509-49e8-b740-8510d27afc96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "0f1c9515-bd1a-48fc-9181-11bd991c70e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "3c65e9ba-1568-4cc7-b029-47ce672157ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "085ddc63-3b05-4aba-bdd9-eececc6efb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 119
        },
        {
            "id": "b43c2d93-d69e-43b6-9a5d-a23618761f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 121
        },
        {
            "id": "a7bac7d2-0a8c-4c27-bda8-26a6a930e493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "6045c6d5-7540-4031-8a6a-f6dcce8ddd30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 894
        },
        {
            "id": "8696ed20-6b23-42d9-80da-5923865491cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "f77b3b8c-3634-420f-be09-0e1dc1ea6510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "86c4fe40-e556-40d7-9535-82871dfb883f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "65325bd8-856d-4d69-9d69-dd49b9cbc2f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "2dae1dd3-1183-49a6-b90c-3e87017dcb88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "94818991-0ef6-49fb-b166-c24281062d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "031cb10d-5c44-4c84-b325-33889baf8290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "a29dc477-86d4-405b-94ca-d59cdc48ad65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "3e355d2d-63db-4ecc-b67b-11ac316e6d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "7d668deb-2c68-4e44-a959-aa6f18b25b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "de855e35-cdb0-457a-87d0-39bb70d74f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "a2568d14-c1be-4dbb-a8b2-c9862fa39ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "08c92452-8739-4cac-8219-460f22a85f23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "7d17257e-268e-41fc-8241-8629511c575f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "de01d3ea-b528-49b7-8d77-186415875587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "b9438698-dfb8-470b-9de4-d6e2ac13b4f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "da84c1da-ce8d-4592-b766-8180ad654fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "97d133c5-2dbc-42e1-8c33-d68d3eba498f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "bcded0c1-58ab-4e8f-bba5-4e4ffbcaba23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "fa9f9877-cd6d-443f-a894-515f13e18bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "292c0411-b6c9-4b69-a5a3-1667cc9f7236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "86bcca48-6ef8-4f2d-9d36-0ae181cdd7e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "7b2ce349-6e88-495a-8ae7-298729b77260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "e3c9ef60-8d85-4fde-a5bb-7511de503b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 58
        },
        {
            "id": "e5e5492d-a288-46b1-927c-28d67326c1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "9959f4ff-ac3f-4948-8c3f-56292dd52ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "f6af4429-a7de-4862-b2df-59650cb60428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "c9b570e1-665b-4155-855a-893af494ae1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "9fc99adc-f461-43ba-a1fd-d71b33a0074e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "379bda46-9020-47aa-a406-e7ce118aae0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "1609481c-1b09-4ea2-91d1-a75edc266e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "b4c88af5-2750-4707-84dd-ee5017b6364a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "8b81ceab-08aa-4c32-9d5f-61b2fd535636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "8d48f46e-b872-4dd5-995c-65e4f595c585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "c7116e17-a5d8-43aa-95bd-0508dd1e83d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "f09913e9-88e5-4594-98f6-bb61ca7176ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "aa7b576a-e57f-4af2-ad61-64131ce59b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "a81e3134-01a3-4052-a908-3ef5c8fc97bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "62723d60-7a90-4a2d-a572-f572e2958ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "709c4845-d949-4ee7-8692-2633c23c9b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "3e01b876-8ac6-49da-a167-2aef491976a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "25d9f4ec-c159-4259-8337-4bdb64787e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "9ae576fb-4a82-43d8-bc5f-261f475173a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "680298ba-170b-4913-9905-e5da7d994bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "0ce22243-63c8-49d1-95ba-c28c7fa5a49a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 20,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}